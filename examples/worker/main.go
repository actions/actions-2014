package main

import "github.com/actionhub/actions/pkg/worker"
import _ "github.com/actionhub/actions/pkg/stdlib/test"

func main() {
	worker.Endpoint("spdy://127.0.0.0.1:8000")
	worker.Start()
}
