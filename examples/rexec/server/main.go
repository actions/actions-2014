package main

import "ioutil"
import "os/exec"
import "github.com/actionhub/actions/pkg/context"
import "github.com/actionhub/actions/pkg/worker"

func command(ctx context.Context) error {
	name := <-ctx.Get("name")
	cmd := exec.Command(name.(string))

	input := <-ctx.Stream("stdin")
	stdin, _ := cmd.StdinPipe()

	go ioutil.Copy(input, stdin)

	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	ctx.Stream("stdout", stdout)
	ctx.Stream("stderr", stderr)

	return cmd.Wait()
}

func main() {
	worker.Endpoint("spdy://127.0.0.0.1:8000")
	worker.Handler("cmd.run", command)
	worker.Start()
}
