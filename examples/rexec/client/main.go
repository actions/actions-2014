package main

func main() {
	context := Call("cmd.run")

	context.Set("name", "ls -ls")
	context.Stream("stdin", os.Stdin)

	stdout := <-context.Stream("stdout")
	stderr := <-context.Stream("stderr")

	go ioutil.Copy(stdout, os.Stdout)
	go ioutil.Copy(stderr, os.Stderr)

	<-context.Error()
}
