# Heartbeat

Heartbeat task requires constant connection and flow of requests end responses.

## Heartbeat server

```Go
func hearbeat(context Context) error {
	for _ = range context.Values() {    // get request
		context.Send("_", true)         // send response
	}
	return nil
}
```

## Heartbeat client

```Go
context := Call("heartbeat")
for _ = range time.Tick(time.Second) {
	context.Send("_", true)         // send request
	health := context.Get("health") // get response
	if health == nil {              // if no response
		break                       // lost heartbeat
	}
}
```
