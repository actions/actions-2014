# actions

Name actions is ambiguous in purpose.

Action describes simple function call chains.

Example action in javascript

```Javascript
http.request({url: 'http://www.google.com/'})
	.then(function(response) {
		html.extract({selectors: {title: "title"}})
			.then(function(response) {
				console.log(title);
			})
	});
```

Example action equivalen in YAML

```YAML
- http.request:
    url: http://www.google.com/
- html.extract:
    selectors:
      title: title
```

Our goal is to make a second version work very efficient.
