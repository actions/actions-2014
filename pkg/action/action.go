package action

import "github.com/crackcomm/go-actions/v2/context"

type Action interface {
	Name() string
	Context() context.Context
	Next() Action
}

// action - Action interface for string.
type action struct {
	name string
	ctx  context.Context
	next Action
}

// New - Creates a new action.
func New(name string, ctx context.Context, next Action) Action {
	return &action{
		name: name,
		ctx:  ctx,
		next: next,
	}
}

// Name - Creates a new action.
func Name(name string) Action {
	return &action{name: name}
}

// Name - Returns action name.
func (s *action) Name() string {
	return s.name
}

// Context - Returns nil.
func (s *action) Context() context.Context {
	return s.ctx
}

// Next - Returns nil.
func (s *action) Next() Action {
	return s.next
}
