package action

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/v2/context"

func TestName(t *T) {
	Convey("Name()", t, func() {
		Convey("Should create action from name", func() {
			a := Name("test")
			So(a.Name(), ShouldEqual, "test")
			So(a.Context(), ShouldEqual, nil)
			So(a.Next(), ShouldEqual, nil)
		})
	})
}

func TestNew(t *T) {
	Convey("New()", t, func() {
		Convey("Should create action with only name", func() {
			a := New("test", nil, nil)
			So(a.Name(), ShouldEqual, "test")
			So(a.Context(), ShouldEqual, nil)
			So(a.Next(), ShouldEqual, nil)
		})
		
		Convey("Should create action with name and context", func() {
			ctx := context.New()
			a := New("test", ctx, nil)
			So(a.Name(), ShouldEqual, "test")
			So(a.Context(), ShouldEqual, ctx)
			So(a.Next(), ShouldEqual, nil)
		})
		
		Convey("Should create action with only name", func() {
			next := New("next", nil, nil)
			ctx := context.New()
			a := New("test", ctx, next)
			So(a.Name(), ShouldEqual, "test")
			So(a.Context(), ShouldEqual, ctx)
			So(a.Next(), ShouldEqual, next)
		})
	})
}
