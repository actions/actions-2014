package context

import "errors"
import . "testing"
import . "github.com/smartystreets/goconvey/convey"

var testError = errors.New("Test error")

func TestNew(t *T) {
	Convey("New()", t, func() {
		Convey("Should create new empty context", func() {
			So(New().Close(), ShouldEqual, nil)
		})
		Convey("Should create new context with values", func() {
			context := New(M{"true": true}, M{"hundred": 100})
			So(<-context.Get("true"), ShouldEqual, true)
			So(<-context.Get("hundred"), ShouldEqual, 100)
			So(context.Close(), ShouldEqual, nil)
		})
	})
}

func TestValues(t *T) {
	Convey("Values()", t, func() {
		Convey("Should return empty channel from closed context", func() {
			context := New()
			So(context.Close(), ShouldEqual, nil)
			So(<-context.Values(), ShouldEqual, nil)
		})
	})
}

func TestGet(t *T) {
	Convey("Get()", t, func() {
		Convey("Should return empty value from closed context", func() {
			context := New()
			So(context.Close(), ShouldEqual, nil)
			So(<-context.Get("test"), ShouldEqual, nil)
		})
	})
}

func TestPop(t *T) {
	Convey("Pop()", t, func() {
		Convey("Should pop value from context", func() {
			context := New()
			context.Set("test", true)
			So(context.Has("test"), ShouldEqual, true)
			So(<-context.Pop("test"), ShouldEqual, true)
			So(context.Has("test"), ShouldEqual, false)
			select {
			case <-context.Pop("test"):
				panic("error")
			default:
			}
			So(context.Close(), ShouldEqual, nil)
			So(context.Has("test"), ShouldEqual, false)
		})
		Convey("Should return empty value from closed context", func() {
			context := New()
			So(context.Close(), ShouldEqual, nil)
			So(<-context.Pop("test"), ShouldEqual, nil)
		})
	})
}

func TestDone(t *T) {
	Convey("Done()", t, func() {
		Convey("Should push error", func() {
			context := New()
			context.Done(testError)
			So(<-context.Error(), ShouldEqual, testError)
			So(context.Close(), ShouldEqual, nil)
		})
	})
}
