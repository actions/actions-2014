package context

// ErrorContext - Creates closed context filled with error.
func ErrorContext(format string, args ...interface{}) Context {
	err := fmt.Errorf(format, args...)
	ctx := &context{}
	ctx.Done(err)
	ctx.Close()
	return ctx
}
