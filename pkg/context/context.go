package context

// Context - Context interface.
type Context interface {
	Has(string) bool
	Get(string) ValueChannel
	Pop(string) ValueChannel
	Set(string, interface{})
	Keys(...string) KeyValueChannel
	Values() KeyValueChannel        // all Keys()
	Error() ErrorChan
	Close() error
	Done(error)
}

// context - Context implementation.
type context struct {
	valueChannels valueChanMap
	keyChannels   []*kvChannel
	popKeys       []string
	values        M
	error         ErrorChan
	closed        bool
}

// New - Creates a new context.
func New(maps ...M) Context {
	c := &context{}
	// Set values from input maps
	if len(maps) == 0 {
		c.values = make(M)
	} else if len(maps) == 1 {
		c.values = maps[0]
	} else if len(maps) > 1 {
		c.values = make(M)
		for _, m := range maps {
			for key, value := range m {
				c.values[key] = value
			}
		}
	}
	return c
}

// Set - Sets value under key.
func (c *context) Set(key string, value interface{}) {
	if c.closed {
		return
	}
	// Save if not popped
	if !c.pop(key) {
		c.values[key] = value
	}
	// Push to all value channels
	if c.valueChannels != nil {
		c.valueChannels.push(key, value)
	}
	// Push to all key-value channels
	if c.keyChannels != nil {
		for _, channel := range c.keyChannels {
			channel.push(key, value)
		}
	}
	return
}

// Has - Checks if has value under key.
func (c *context) Has(key string) bool {
	return c.values[key] != nil
}

// Get - Gets value from context.
func (c *context) Get(key string) ValueChannel {
	return c.value(key, false)
}

// Pop - Pops value from context.
func (c *context) Pop(key string) ValueChannel {
	return c.value(key, true)
}

// Values - Creates a channel that will watch for all key-values inserted into context.
func (c *context) Values() KeyValueChannel {
	return c.Keys()
}

// Values - Creates a channel that will watch for all keys on the list.
func (c *context) Keys(keys ...string) KeyValueChannel {
	channel := newKeyValueChannel(keys...)

	// Add all keys already here
	for _, key := range keys {
		if value := c.values[key]; value != nil {
			channel.push(key, value)
		}
	}

	// Append to watchers list if not closed
	if !c.closed {
		c.keyChannels = append(c.keyChannels, channel)
	} else {
		close(channel.channel)
	}

	return channel.channel
}

// value - Gets or pops value from context.
func (c *context) value(key string, pop bool) ValueChannel {
	// If pop enabled add to popKeys
	if pop && !c.pop(key) {
		c.popKeys = append(c.popKeys, key)
	}

	// If value already in context - return filled channel
	if value := c.values[key]; value != nil {
		// Delete value from context if pop
		if pop {
			delete(c.values, key)
		}
		return filledChannel(value)
	}

	// If closed return empty value
	if c.closed {
		return filledChannel(nil)
	}

	// Create value channel
	channel := make(ValueChannel, ChannelSize)

	// Create value channels list
	if c.valueChannels == nil {
		c.valueChannels = make(valueChanMap)
	}

	// Add channel to the list
	c.valueChannels[key] = append(c.valueChannels[key], channel)

	return channel
}

// pop - Checks if key is on c.popKeys list.
func (c *context) pop(key string) bool {
	for _, k := range c.popKeys {
		if k == key {
			return true
		}
	}
	return false
}

// Close - Closes all channels.
func (c *context) Close() error {
	// Set context closed
	c.closed = true

	// Close all value channels
	if c.valueChannels != nil {
		c.valueChannels.close()
	}

	// Close all key-value channels
	for _, channel := range c.keyChannels {
		close(channel.channel)
	}

	// Remove them
	c.valueChannels = nil
	c.keyChannels = nil
	return nil
}

// Done - Pushes error to context error channel.
func (c *context) Done(err error) {
	if c.error == nil {
		c.error = make(ErrorChan, 1)
	}
	c.error <-err
}

// Error - Returns error channel.
func (c *context) Error() ErrorChan {
	if c.error == nil {
		c.error = make(ErrorChan, 1)
	}
	return c.error
}
