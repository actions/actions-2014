package context

// EmptyContext - Creates an empty, closed context.
func EmptyContext() Context {
	ctx := &context{}
	ctx.Done(nil)
	ctx.Close()
	return ctx
}
