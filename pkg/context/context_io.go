package context

import "io"

// contextIO - I/O Context.
type contextIO struct {
	input  Context
	output Context
}

// NewIO - Creates a new I/O Context.
func NewIO(input, output Context) Context {
	return &contextIO{
		input:  input,
		output: output,
	}
}

// Has - Checks if has input key.
func (c *contextIO) Has(key string) bool {
	return c.input.Has(key)
}

// Set - Sets output variable.
func (c *contextIO) Set(key string, value interface{}) {
	c.output.Set(key, value)
}

// Get - Gets input variable.
func (c *contextIO) Get(key string) ValueChannel {
	return c.input.Get(key)
}

// Pop - Pops input variable.
func (c *contextIO) Pop(key string) ValueChannel {
	return c.input.Pop(key)
}

// Keys - Gets all input keys.
func (c *contextIO) Keys(keys ...string) KeyValueChannel {
	return c.input.Keys(keys...)
}

// Values - Gets all input variables.
func (c *contextIO) Values() KeyValueChannel {
	return c.input.Values()
}

// Error - Gets input error.
func (c *contextIO) Error() values.ErrorChan {
	return c.input.Error()
}

// Done - Sends error to output context.
func (c *contextIO) Done(err ...error) {
	c.output.Done(err...)
}

// Stream - Gets input stream or adds output stream.
// func (c *contextIO) Stream(key string, writers ...io.ReadCloser) chan io.ReadCloser {
// 	if len(writers) == 0 {
// 		return c.input.Stream(key)
// 	}
// 	return c.output.Stream(key, writers...)
// }

// Close - Closes In and Out contexts.
func (c *contextIO) Close() error {
	c.output.Close()
	c.input.Close()
	return nil
}
