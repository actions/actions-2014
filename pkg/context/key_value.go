package context

// KeyValue - Key and Value, together.
type KeyValue struct {
	Key   string
	Value interface{}
}

// KeyValueChannel - Channel transporting key and value.
type KeyValueChannel chan *KeyValue

// kvChannel - Structure that allows only specified keys to be pushed into a channel.
type kvChannel struct {
	channel KeyValueChannel
	keys    []string
	all     bool
}

// newKeyValueChannel - Creates a new kvChannel.
func newKeyValueChannel(keys ...string) *kvChannel {
	ch := &kvChannel{
		channel: make(KeyValueChannel, ChannelSize),
		keys:    keys,
	}
	if len(keys) == 0 {
		ch.all = true
	}
	return ch
}

// push - Pushes key-value to a channel if pushing all or key is on the list.
func (kv *kvChannel) push(key string, value interface{}) {
	if kv.all || kv.has(key) {
		kv.channel <-&KeyValue{Key: key, Value: value}
	}
}

// has - Checks if key is on kv.keys list.
func (kv *kvChannel) has(key string) bool {
	for _, k := range kv.keys {
		if k == key {
			return true
		}
	}
	return false
}
