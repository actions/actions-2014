package context

// M - Map of values.
type M map[string]interface{}

// ValueChannel - Channel transporting value.
type ValueChannel chan interface{}

// ErrorChan - Channel transporting error.
type ErrorChan chan error

// ChannelSize - Default size of every channel except filled and error channel.
var ChannelSize = 16

// valueChanMap - Map of ValueChannels.
type valueChanMap map[string][]ValueChannel

// filledChannel - Returns ValueChannel filled with value.
func filledChannel(value interface{}) ValueChannel {
	channel := make(ValueChannel, 1)
	channel <-value
	close(channel)
	return channel
}

// push - Push value to all channels under key.
func (m valueChanMap) push(key string, value interface{}) {
	for _, channel := range m[key] {
		channel <-value
		close(channel)
	}
}

// close - Close all channels
func (m valueChanMap) close() {
	for _, channels := range m {
		for _, channel := range channels {
			close(channel)
		}
	}
}
