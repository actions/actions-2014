package test

import "github.com/actionhub/pkg/core"
import "github.com/actionhub/pkg/context"

// Ping - Sends a ping.
func Ping(ctx context.Context) error {
	ctx.Set("ping", "pong")
	return nil
}

func init() {
	core.Handler("test.ping", Ping)
}
