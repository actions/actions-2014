package core

import "runtime"
import "github.com/actionhub/actions/pkg/context"

// core - Core implementation.
type core struct {
	functions map[string]Function
}

// call - Calls a function by name with input context.
func (c *core) call(name string, input context.Context) context.Context {
	function := c.functions[name]
	if function == nil {
		return context.ErrorContext("Function %s was not found", name)
	}
	return call(function, input)
}

// call - Calls a function with input context.
func call(function Function, input context.Context) context.Context {
	if input == nil {
		input = context.EmptyContext()
	}
	ctx := context.ContextIO(input, output)
	go run(function, ctx)
	runtime.Gosched()
	return ctx
}

// run - runs a function with context then marks it as done.
func run(function Function, ctx context.Context) {
	err := function(ctx)
	ctx.Done(err)
}
